import { createRouter, createMemoryHistory, createWebHistory } from 'vue-router';
import Profile from '../components/Profile.vue';
import Index from '../views/Index.vue';

const isServer = typeof window === 'undefined';
const history = isServer ? createMemoryHistory() : createWebHistory();
const routes = [
    {
        path: '/',
        name: 'Index',
        component: Index,
    },
    {
        path: '/',
        name: 'About',
        component: Index,
    },
    {
        path: '/',
        name: 'Contacts',
        component: Index,
    },
    {
        path: '/',
        name: 'Delivery',
        component: Index,
    },
    {
        path: '/profile',
        name: 'Profile',
        component: Profile,
    },
];

const router = createRouter({
    history,
    routes,
});

export default router;
